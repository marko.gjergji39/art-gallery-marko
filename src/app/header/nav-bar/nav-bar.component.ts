import {Component, OnInit, Output, EventEmitter} from '@angular/core';


type Item = {name: string, selected: boolean};

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

    
    navbarItems: Item[] = [
        { name: 'Painting', selected: true },
        { name: 'Sculpture', selected: false },
        { name: 'Photography', selected: false },
        { name: 'Artists', selected: false },
        { name: 'Collections', selected: false}
    ];

    @Output() selectedItem : EventEmitter<string> = new EventEmitter<string>()

    constructor() {
    }

    ngOnInit(): void {
    }

    onSelectItem(item: Item): void {
        this.selectedItem.emit(item.name);
        this.navbarItems.map((el:Item) => el.selected = el.name===item.name);
    }

}
