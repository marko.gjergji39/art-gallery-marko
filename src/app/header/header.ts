import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AppRoutingModule } from '../app-routing.module';
import { SidebarComponent } from './sidebar/sidebar.component';



@NgModule({
  declarations: [
    NavBarComponent,
    SidebarComponent
  ],
  exports:[
      NavBarComponent,
      SidebarComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
  ]
})
export class HeaderModule { }
