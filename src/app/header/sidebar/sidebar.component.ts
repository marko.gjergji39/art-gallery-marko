import { Component, OnInit } from '@angular/core';
import { ActionsService } from 'src/app/_services/actions.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(public actionsService : ActionsService) { }

  ngOnInit(): void {
  }

}
