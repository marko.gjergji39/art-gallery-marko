import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoContainerComponent } from './photo-container/photo-container.component';
import { PaintingComponent } from './painting/painting.component';
import { AddArtComponent } from './add-art/add-art.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NewArtistComponent } from './new-artist/new-artist.component';
import { HomeComponent } from './home/home.component';
import { HeaderModule } from '../header/header';
import { DisplayArtistsComponent } from './display-artists/display-artists.component';
import { AppRoutingModule } from '../app-routing.module';
import { CollectionsComponent } from './collections/collections.component';



@NgModule({
  declarations: [
    AddArtComponent,
    PaintingComponent,
    PhotoContainerComponent,
    NewArtistComponent,
    HomeComponent,
    DisplayArtistsComponent,
    CollectionsComponent
  ],
  exports:[
    AddArtComponent,
    PaintingComponent,
    PhotoContainerComponent,
    DisplayArtistsComponent,
    CollectionsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderModule,
    AppRoutingModule
  ]
})
export class PhotoGalleryModule { }
