import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-photo-container',
  templateUrl: './photo-container.component.html',
  styleUrls: ['./photo-container.component.css']
})
export class PhotoContainerComponent implements OnInit {

    @ViewChild("container") container! : ElementRef;
    @ViewChild("canvas") canvas! : ElementRef;
    constructor() { }

    ngOnInit(): void {
    }
    
    @HostListener('window:mousemove', ['$event'])
    onMouseMove(event: MouseEvent){
       let x = event.clientX - this.container.nativeElement.getBoundingClientRect().left;
       let y = event.clientY - this.container.nativeElement.getBoundingClientRect().top;

       this.canvas.nativeElement.style.transform = `translate(-${x}px , -${y * 2}px)`
    }
}
