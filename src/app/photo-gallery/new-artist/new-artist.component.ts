import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ArtistsMock } from 'src/app/_mock-data/artists.mock';

@Component({
  selector: 'app-new-artist',
  templateUrl: './new-artist.component.html',
  styleUrls: ['./new-artist.component.css']
})
export class NewArtistComponent implements OnInit {

  
    myForm: FormGroup;

    constructor(private fb: FormBuilder, private router: Router) {
      this.myForm = this.fb.group({
        firstName: new FormControl('', [Validators.required, Validators.minLength(3)] ),
        lastName: new FormControl('', [Validators.required, Validators.minLength(3)]),
        password: new FormControl('', [Validators.required, Validators.minLength(8), this.checkPassword.bind(this)]),
        repeatPassword: new FormControl('', [Validators.required, Validators.minLength(8), this.checkPassword.bind(this)]),
        email: new FormControl('', [Validators.required,	Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
        birthday: new FormControl('', [ Validators.required, this.checkDate.bind(this)]),
        gender: new FormControl(),
        category: new FormControl(),
        description: new FormControl()
      })
    }
  
    ngOnInit(): void {
    }
  
    checkDate(formControl: any): any {
      if (new Date(formControl.value).getFullYear() > 2003) {
        return {error: 'FormError'}
      }
    }
  
    logForm() {
      const artist = {
        firstName: this.myForm.get('firstName')?.value,
        lastName: this.myForm.get('lastName')?.value,
        password: this.myForm.get('password')?.value,
        email: this.myForm.get('email')?.value,
        birthday: this.myForm.get('birthday')?.value,
        gender: this.myForm.get('gender')?.value,
        category: this.myForm.get('category')?.value,
        description: this.myForm.get('description')?.value,
  
      };
      ArtistsMock.push(artist);
      this.router.navigate(["/home/artists"])
    }
  
    navigateToList() {
      this.router.navigate(['artists-list']);
    }
  
    checkPassword(val:any): any {
  
      if(this.myForm) {
        const pass = this.myForm.get('password')?.value;
        const repeatPass = this.myForm.get('repeatPassword')?.value;
        if (pass  && repeatPass  && pass !== repeatPass) {
          return {error: 'Password Error'}
        }
      }
      return null;
  
    }
}
