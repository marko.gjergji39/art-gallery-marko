import { Component, OnChanges, OnInit } from '@angular/core';
import {PhotoModel} from "../../_models/photo-model";
import {PaintingMock, PhotographyMock, SculptureMock} from "../../_mock-data/photo-mock";
import { ActionsService } from 'src/app/_services/actions.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'app-painting',
    templateUrl: './painting.component.html',
    styleUrls: ['./painting.component.css']
})


export class PaintingComponent implements OnInit,OnChanges {

    paintings = PaintingMock;
    
    constructor(public actionsService : ActionsService,private route : ActivatedRoute,private router: Router) {
        
        
        this.route.url.subscribe(params => {
            switch (this.router.url) {
                case "/home/paintings":
                    this.paintings = PaintingMock
                    break;
                case "/home/sculptures":
                    this.paintings = SculptureMock
                    break;
                case "/home/photographies":
                    this.paintings = PhotographyMock
                    break;
                
                default:
                    break;
            }
          })
    }

    ngOnInit(): void {
        
    }

    ngOnChanges() {
        
    }
  
   
}
