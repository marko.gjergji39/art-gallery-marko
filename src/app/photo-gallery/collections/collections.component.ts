import { Component, OnInit } from '@angular/core';
import { ActionsService } from 'src/app/_services/actions.service';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.css']
})
export class CollectionsComponent implements OnInit {

  constructor(public actionsService : ActionsService) { }

  ngOnInit(): void {
  }

}
