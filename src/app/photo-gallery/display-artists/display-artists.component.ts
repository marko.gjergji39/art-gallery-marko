import { Component, OnInit } from '@angular/core';
import { ArtistsMock } from 'src/app/_mock-data/artists.mock';

@Component({
  selector: 'app-display-artists',
  templateUrl: './display-artists.component.html',
  styleUrls: ['./display-artists.component.css']
})
export class DisplayArtistsComponent implements OnInit {

    artists : Array<any> = [];

    constructor() {
        this.artists = ArtistsMock;
    }

    ngOnInit(): void {
    }


}
