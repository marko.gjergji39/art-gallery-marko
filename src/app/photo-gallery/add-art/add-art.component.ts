import { Component,EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaintingMock } from 'src/app/_mock-data/photo-mock';
import { PhotographyMock } from 'src/app/_mock-data/photo-mock';

@Component({
  selector: 'app-add-art',
  templateUrl: './add-art.component.html',
  styleUrls: ['./add-art.component.css']
})
export class AddArtComponent {

    categories: Array<string> = ['Painting','Sculpture','Photography', 'Artists', 'Collections']
   
    myform: FormGroup;
    artCategory:FormControl;
    title: FormControl;
    artist: FormControl;
    technique: FormControl;
    dimension: FormControl;
    price: FormControl;
    pictureUrl: FormControl;
    category: FormControl;
    submitted = false;

    @Output() loggedIn : EventEmitter<string> = new EventEmitter<string>();
  
    constructor() {
        
        this.title = new FormControl('', Validators.required);
        this.artCategory = new FormControl('', Validators.required);
        this.artist = new FormControl('', Validators.required);
        this.price = new FormControl('', Validators.required);
        this.technique = new FormControl('', Validators.required);
        this.dimension = new FormControl('', Validators.required);
        this.pictureUrl = new FormControl('', Validators.required);
        this.category = new FormControl('', Validators.required);

        this.myform = new FormGroup({
            
            title: this.title,
            artCategory: this.artCategory,
            artist: this.artist,
            price: this.price,
            technique: this.technique,
            dimension: this.dimension,
            pictureUrl: this.dimension,
            category: this.dimension
        });

        

    }

    onSubmit() {

        switch (this.myform.value.artCategory) {
            case this.categories[0]:
                PaintingMock.push(this.myform.value)
                break;
            case this.categories[2]:
                PhotographyMock.push(this.myform.value)
                break;
            default:
                break;
        }

    }
}
