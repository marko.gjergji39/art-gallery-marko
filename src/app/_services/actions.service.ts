import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

    favorites : Array<any> = [];
    cart : Array<any> = [];
    favoriteItems = [];
    loggedIn = false;
    
    constructor() { }

    onSearchClick(url: string | undefined):void {
        if(url) {
        window.open(url);
        }
    }

    onBuyClick(painting: any) {
        this.cart.indexOf(painting) > -1 ? this.cart.splice(this.cart.indexOf(painting),1) : this.cart.push(painting)
    }

    favorite(painting:any){
        this.favorites.indexOf(painting) > -1 ? this.favorites.splice(this.favorites.indexOf(painting),1) : this.favorites.push(painting)
        
    }
    isFavorite(painting:any) : boolean{
        return this.favorites.indexOf(painting) > -1 ? true : false
    }
    isInCart(painting:any) : boolean{
        return this.cart.indexOf(painting) > -1 ? true : false
    }

}
