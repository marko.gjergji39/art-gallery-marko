import { Component } from '@angular/core';

@Component({
  selector: 'main-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'art-gallery-fc';

  open : boolean = true;

  clicks : Array<number> =  [];

  differentStyle : boolean = false;
  show(){
      
      this.open = !this.open;
      this.clicks.push(1);
      if(this.clicks.length >= 5){
          this.differentStyle = true;
      }
  }
}
