export interface PhotoModel {
  title?: string,
  artist?: string,
  creationDate?: Date,
  technique?: string,
  price?: number,
  dimension?: string,
  pictureUrl?: string,
  category?: string
}
