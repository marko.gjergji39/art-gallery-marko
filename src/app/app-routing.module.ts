import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './authenticate/login/login.component';
import { RegisterComponent } from './authenticate/register/register.component';
import { MainComponent } from './main/main.component';
import { CollectionsComponent } from './photo-gallery/collections/collections.component';
import { DisplayArtistsComponent } from './photo-gallery/display-artists/display-artists.component';
import { HomeComponent } from './photo-gallery/home/home.component';
import { NewArtistComponent } from './photo-gallery/new-artist/new-artist.component';
import { PaintingComponent } from './photo-gallery/painting/painting.component';
import { WelcomeComponent } from './startup/welcome/welcome.component';
import { AuthGuard } from './_services/auth.guard';

const routes: Routes = [
    { path: '', component: WelcomeComponent},
    { path: 'login', component: LoginComponent},
    { path: 'register', component: RegisterComponent},
    { path: 'home', component: MainComponent, canActivate:[AuthGuard],
    children: [
        {path: 'paintings', component: PaintingComponent, canActivate:[AuthGuard]},
        {path: 'sculptures', component: PaintingComponent, canActivate:[AuthGuard]},
        {path: 'photographies', component: PaintingComponent, canActivate:[AuthGuard]},
        {path: 'artists', component: DisplayArtistsComponent, canActivate:[AuthGuard]}, 
        {path: 'add-artist', component: NewArtistComponent, canActivate:[AuthGuard]},
        {path: 'collections', component: CollectionsComponent, canActivate:[AuthGuard]}
        /* , canActivate: [AuthGuard]
         
        /* {path: 'collections', component: LoginComponent *//* , canActivate: [AuthGuard] }*/
      ]}
    /* { path: '**', component: NotFoundComponent} */
    
  ];
  

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
