import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

    category:String = '';
    loggedIn = false;
    username = "";
    constructor() { }

    ngOnInit(): void {
    }

    showCategory(val : any): void {
        console.log(val);
        this.category = val;
    }

    login(val : string):void{
        this.loggedIn = true;
        this.username = val;
    }
}
