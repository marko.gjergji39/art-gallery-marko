import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header';
import { PhotoGalleryModule } from '../photo-gallery/photo-gallery.module';
import { PaintingComponent } from '../photo-gallery/painting/painting.component';

import { MainComponent } from './main.component';
import { AuthenticateModule } from '../authenticate/authenticate.module';
import { AppRoutingModule } from '../app-routing.module';




@NgModule({
  declarations: [
      MainComponent,
  ],
  exports:[
      MainComponent
  ],
  imports: [
    CommonModule,
    PhotoGalleryModule,
    AuthenticateModule,
    AppRoutingModule,
  ]
})
export class MainModule { }
