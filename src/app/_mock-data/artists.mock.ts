
export const ArtistsMock = [
  {
    firstName: 'Marko',
    lastName: 'Marko',
    email: 'marko@marko.com',
    birthday: '03.09.1999',
    gender: 'male',
    category: 'Painting',
    description:'Artist'
  }
];
