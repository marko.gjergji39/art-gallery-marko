import {PhotoModel} from "../_models/photo-model";

export let PaintingMock: PhotoModel[] = [{
  title: 'The Well Out Front',
  artist: 'Stephanie Berry',
  creationDate: new Date(2017),
  technique: 'Oil on Linen',
  price: 1800,
  dimension: '30x40in',
  pictureUrl: './assets/paintings/picture1.jpg',
  category: 'landscape'
},{
  title: 'The Well Out Front',
  artist: 'Stephanie Berry',
  creationDate: new Date(2017),
  technique: 'Oil on Linen',
  price: 1800,
  dimension: '30x40in',
  pictureUrl: './assets/paintings/picture1.jpg',
  category: 'landscape'
},{
  title: 'The Well Out Front',
  artist: 'Stephanie Berry',
  creationDate: new Date(2017),
  technique: 'Oil on Linen',
  price: 1800,
  dimension: '30x40in',
  pictureUrl: './assets/paintings/picture1.jpg',
  category: 'landscape'
}, {
  title: 'The Well Out Front',
  artist: 'Stephanie Berry',
  creationDate: new Date(2017),
  technique: 'Oil on Linen',
  price: 1800,
  dimension: '30x40in',
  pictureUrl: './assets/paintings/picture1.jpg',
  category: 'landscape'
},{
  title: 'The Well Out Front',
  artist: 'Stephanie Berry',
  creationDate: new Date(2017),
  technique: 'Oil on Linen',
  price: 1800,
  dimension: '30x40in',
  pictureUrl: './assets/paintings/picture1.jpg',
  category: 'landscape'
},{
  title: 'The Well Out Front',
  artist: 'Stephanie Berry',
  creationDate: new Date(2017),
  technique: 'Oil on Linen',
  price: 1800,
  dimension: '30x40in',
  pictureUrl: './assets/paintings/picture1.jpg',
  category: 'landscape'
}

];
export let SculptureMock: PhotoModel[] = [{
    title: 'Plaster casts',
    artist: 'Lorenzo Bartolini',
    creationDate: new Date(2019),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: '../assets/images/sculpture.jpg',
    category: 'landscape'
  },{
    title: 'Plaster casts',
    artist: 'Lorenzo Bartolini',
    creationDate: new Date(2019),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: '../assets/images/sculpture.jpg',
    category: 'landscape'
  },{
    title: 'Plaster casts',
    artist: 'Lorenzo Bartolini',
    creationDate: new Date(2019),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: '../assets/images/sculpture.jpg',
    category: 'landscape'
  }, {
    title: 'Plaster casts',
    artist: 'Lorenzo Bartolini',
    creationDate: new Date(2019),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: '../assets/images/sculpture.jpg',
    category: 'landscape'
  },{
    title: 'Plaster casts',
    artist: 'Lorenzo Bartolini',
    creationDate: new Date(2019),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: '../assets/images/sculpture.jpg',
    category: 'landscape'
  },{
    title: 'Plaster casts',
    artist: 'Lorenzo Bartolini',
    creationDate: new Date(2019),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: '../assets/images/sculpture.jpg',
    category: 'landscape'
  }
  
  ];
export let PhotographyMock: PhotoModel[] = [{
    title: 'Birds',
    artist: 'Stephanie Berry',
    creationDate: new Date(2017),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: './assets/images/photo.jpg',
    category: 'landscape'
  },{
    title: 'Birds',
    artist: 'Stephanie Berry',
    creationDate: new Date(2017),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: './assets/images/photo.jpg',
    category: 'landscape'
  },{
    title: 'Birds',
    artist: 'Stephanie Berry',
    creationDate: new Date(2017),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: './assets/images/photo.jpg',
    category: 'landscape'
  }, {
    title: 'Birds',
    artist: 'Stephanie Berry',
    creationDate: new Date(2017),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: './assets/images/photo.jpg',
    category: 'landscape'
  },{
    title: 'Birds',
    artist: 'Stephanie Berry',
    creationDate: new Date(2017),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: './assets/images/photo.jpg',
    category: 'landscape'
  },{
    title: 'Birds',
    artist: 'Stephanie Berry',
    creationDate: new Date(2017),
    technique: 'Oil on Linen',
    price: 1800,
    dimension: '30x40in',
    pictureUrl: './assets/images/photo.jpg',
    category: 'landscape'
  }
  
  ];
  
