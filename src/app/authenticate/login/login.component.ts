import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionsService } from 'src/app/_services/actions.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent {

    exists = false;
    login = false;
    model = {
        username : "",
        password : ""
    }
    username = "";
    submitted = false;
    @Output() loggedIn : EventEmitter<string> = new EventEmitter<string>();

    constructor(public actionsService : ActionsService,public router : Router){
        
    }
    
    onSubmit(userForm : NgForm) {
        console.log(userForm.value);

        let a = JSON.parse(localStorage.getItem('user') || '[]');
        
        a.forEach((user : any) => {
            console.log(user);
            if(userForm.value.username === user.name.username){
                if(userForm.value.password === user.password){
                    this.exists=true;
                    this.login=true;
                    this.username = userForm.value.username;
                }else{
                    this.exists=true;
                }
            }
        });
    
        if(this.login){
            this.actionsService.loggedIn = true;
            this.router.navigate(["/home/paintings"])
        }
        

    }

    
}
