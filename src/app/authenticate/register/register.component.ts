import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionsService } from 'src/app/_services/actions.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

    myform: FormGroup;
    firstName: FormControl;
    lastName: FormControl;
    email: FormControl;
    password: FormControl;
    username: FormControl;
    submitted = false;

    @Output() loggedIn : EventEmitter<string> = new EventEmitter<string>();
  
    constructor(public actionsService : ActionsService,public router : Router) {
        
        this.firstName = new FormControl('', Validators.required);
        this.lastName = new FormControl('', Validators.required);
        this.username = new FormControl('', Validators.required);
        this.email = new FormControl('', [
            Validators.required,
            Validators.pattern("[^ @]*@[^ @]*")
        ]);
        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(8)
        ]);


        this.myform = new FormGroup({
            name: new FormGroup({
            firstName: this.firstName,
            lastName: this.lastName,
            username: this.username,
            }),
            email: this.email,
            password: this.password
        });


    }

    onSubmit() {

        let a = [];

        a = JSON.parse(localStorage.getItem('user') || '[]');

        a.push(this.myform.value);

        localStorage.setItem('user', JSON.stringify(a));

        this.actionsService.loggedIn = true;
        this.router.navigate(["/home/paintings"])

    }
  
}
